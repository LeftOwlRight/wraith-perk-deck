Hooks:PostHook(BlackMarketTweakData,"_init_projectiles","ghost_skill_1_init_throwable",function(self,_tweak_data)
	self.projectiles.ghost_skill_1 = {
		name_id = "bm_grenade_ghost",
		desc_id = "bm_grenade_ghost_desc",
		icon = "dada_com",
		ability = "ghost_skill_1",
		custom = true,
		ignore_statistics = true,
		based_on = "dada_com",
		texture_bundle_folder = "ghost",
		max_amount = 1,
		base_cooldown = 21,
		sounds = {
			activate = "perkdeck_activate",
			cooldown = "perkdeck_cooldown_over"
		}
	}
	table.insert(self._projectiles_index,"ghost_skill_1")
end)



Hooks:PostHook(UpgradesTweakData,"_grenades_definitions","ghost_grenades_definitions",function(self)
	self.definitions.ghost_skill_1 = {
		category = "grenade"
	}
end)
