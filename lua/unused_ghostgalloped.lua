--9th deck half

local stoptime = 0
local gal_value = 0

--be red
if WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
    WFHud.skill_map.player.ghost_galloped.is_debuff = true
end

Hooks:PostHook(PlayerDamage,"update","GhostGallopedUpdate",function(self,unit,t,dt)
    if not Utils:IsInHeist() then
        return
    end
    stoptime = stoptime + dt
    --gal_value = 7 - stoptime
    --WFHud:add_buff("player", "ghost_galloped", gal_value)

    if stoptime >= 7 then
        if managers.player:has_category_upgrade("player","ghost_galloped") then
            managers.player:player_unit():character_damage():set_god_mode(true)
            --WFHud:remove_buff("player", "ghost_galloped")
            gal_value = 9 - stoptime
            if WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
                WFHud.skill_map.player.ghost_galloped.is_debuff = false
            end
            WFHud:add_buff("player", "ghost_galloped", gal_value)
        end
    end
    if stoptime >= 9 then
        WFHud:remove_buff("player", "ghost_galloped")
        --if WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
            --WFHud.skill_map.player.ghost_galloped.is_debuff = true
        --end
        stoptime = 0
        managers.player:player_unit():character_damage():set_god_mode(false)
    end

end)
