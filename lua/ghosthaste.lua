is_ghost_haste = -1
is_ghost_save = -1

-- -1 off  >-1 on

-- cd
ghost_haste_CD = 21
ghost_save_CD = 21

-- buff cd
ghost_haste_buffer_CD = 3
ghost_save_buffer_CD = 3

--punishment cd
punishment_cd = 1

--9th deck
local stoptime = 0
local gal_yu = 0
local gal_duration = 0
local gal_value = 0
local gal_value_t = 0


if WFHud and HUDBuffList then
function HUDBuffList:add_buff(upgrade_data, value, duration)
    --log(tostring(upgrade_data.name_id), tostring(upgrade_data.key))
    local buff = self:_get_existing_buff(upgrade_data)
    if buff and not buff._expired then
        buff:set_values(value, duration)
        return
    end
    buff = HUDBuffListItem:new(self._panel, upgrade_data, value, duration)
    table.insert(self.buff_list, buff)
    buff:set_index(#self.buff_list)
    self:_set_buff(upgrade_data, buff)
    if table.size(self._buff_map[upgrade_data.name_id]) > 1 then
        for _, b in pairs(self._buff_map[upgrade_data.name_id]) do
            b:set_category_icon_visibility(true)
        end
    end
end

function HUDBuffList:remove_buff(upgrade_data)
    local buff = self:_get_existing_buff(upgrade_data)
    if buff then
        buff._expired = true
        self:update()
    end
end
end





if RequiredScript == "lib/units/beings/player/states/playerstandard" then
    backUpSpeed = PlayerStandard._get_max_walk_speed
end



function setplayermaxarmor()
    managers.player:player_unit():character_damage():set_armor(managers.player:player_unit():character_damage():_max_armor())
	managers.player:player_unit():character_damage():_send_set_armor()
end

Hooks:PostHook(PlayerDamage, "on_downed", "resettime", function(self)
    if is_ghost_haste ~= -1 then
        is_ghost_haste = -1
        DelayedCalls:Add( "DelayedCallsReturnHaste", 1 , function()
            is_ghost_haste = -1
        end )
    end

    if is_ghost_save ~= -1 then
        DelayedCalls:Add( "DelayedCallsReturnSave", 1 , function()
            is_ghost_save = -1
        end )
    end

end)



if RequiredScript == "lib/units/beings/player/playerdamage" then

--be red



Hooks:PostHook(PlayerDamage,"update","GhostGallopedUpdate1",function(self,unit,t,dt)

    if not Utils:IsInHeist() then
        return
    end

    stoptime = stoptime + dt
    if  managers.player:player_unit():character_damage():_max_armor() and  managers.player:player_unit():character_damage()._max_armor then 
        punishment_cd = managers.player:player_unit():character_damage():_max_armor()
    end
    gal_duration = 7 - stoptime

    if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
        WFHud.skill_map.player.ghost_galloped.is_debuff = true
    end
    if GhostPerkDeckVinight.settings.ltyr_hud == 1 then
        if WFHud then
            WFHud:add_buff("player", "ghost_galloped", gal_value_t, gal_duration)
        end
    elseif GhostPerkDeckVinight.settings.ltyr_hud == 2 then
        WFHud:add_buff("player", "ghost_galloped", gal_value_t, gal_duration)
    end

    if punishment_cd > 6 and punishment_cd< 10 then
        punishment_cd = 0.5
    elseif punishment_cd > 10 and punishment_cd < 20 then
        punishment_cd = 0.8

    elseif punishment_cd > 20 then
        punishment_cd = 1
    else
        punishment_cd = 0.25
    end

    gal_value_t = 21*punishment_cd
    --gal_yu = gal_value % 1
    --gal_value_t = gal_value - gal_yu

    if stoptime >= 7 and managers.player:has_category_upgrade("player","ghost_galloped")then
        managers.player:player_unit():character_damage():set_god_mode(true)
        
        if WFHud then
            WFHud:remove_buff("player", "ghost_galloped")
        end

        gal_duration = 9 - stoptime

        if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
            WFHud.skill_map.player.ghost_galloped.is_debuff = false
        end

        if GhostPerkDeckVinight.settings.ltyr_hud == 1 then
            if WFHud then
                WFHud:add_buff("player", "ghost_galloped", gal_value_t, gal_duration)
            end
        elseif GhostPerkDeckVinight.settings.ltyr_hud == 2 then
            WFHud:add_buff("player", "ghost_galloped", gal_value_t, gal_duration)
        end
    end
    if stoptime >= 9 and  managers.player:has_category_upgrade("player","ghost_galloped") then
        if GhostPerkDeckVinight.settings.ltyr_hud == 1 or GhostPerkDeckVinight.settings.ltyr_hud == 2 then
            if WFHud then
                WFHud:remove_buff("player", "ghost_galloped")
                if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_galloped then
                    WFHud.skill_map.player.ghost_galloped.is_debuff = true
                end
            end
        end
        stoptime = 0
        managers.player:player_unit():character_damage():set_god_mode(false)
    end

end)

end  --if RequireScript

Hooks:PostHook(PlayerDamage,"update","GhosthasteUpdate",function(self,unit,t,dt)

    if not Utils:IsInHeist() then
        return
    end

    -- 判断是否为on
    if is_ghost_haste > -1 then
        is_ghost_haste = is_ghost_haste + dt
    end
    if is_ghost_save > -1 then
        is_ghost_save = is_ghost_save + dt
    end

    -- 结束buff
    if is_ghost_haste > ghost_haste_CD then
        is_ghost_haste = -1
        punishment_cd = 1
        --managers.chat:_receive_message(1,'灵魂疾走','cd已重置',Color.red)
    end
    if is_ghost_save > ghost_save_CD then
        is_ghost_save = -1
        punishment_cd = 1
        --managers.chat:_receive_message(1,'灵魂保护','cd已重置',Color.green)

    end

    if is_ghost_haste == -1 and is_ghost_save == -1 and managers.player:player_unit():character_damage():get_real_armor() <= 0 and not managers.player:player_unit():character_damage():is_downed()
    and managers.player:has_category_upgrade("player","ghost_galloped")
    then
        -- 让自动保护返还cd
        if  managers.player:player_unit():character_damage():_max_armor() and  managers.player:player_unit():character_damage()._max_armor then 
            punishment_cd = managers.player:player_unit():character_damage():_max_armor()
        end

        if punishment_cd > 6 and punishment_cd< 10 then
            punishment_cd = 0.5
        elseif punishment_cd > 10 and punishment_cd < 20 then
            punishment_cd = 0.8

        elseif punishment_cd > 20 then
            punishment_cd = 1
        else
            punishment_cd = 0.25
        end

        is_ghost_haste = ghost_haste_CD - ghost_haste_CD * punishment_cd
        is_ghost_save = ghost_haste_CD - ghost_save_CD * punishment_cd


        setplayermaxarmor()
    end

end)





Hooks:PostHook(CopDamage,"_on_death", "killtime", function(self)
    if not Utils:IsInHeist() then
        return
    end
    if is_ghost_haste > -1 then
        is_ghost_haste = is_ghost_haste + 1
    end
    if is_ghost_save > -1 then
        is_ghost_save = is_ghost_save + 1
    end
end)





---- 这里控制彩蛋画面 --------


easterEgg_active = false

easterEgg_effect = "color_sin_classic"


local old_set_post_composite = old_set_post_composite or CoreEnvironmentControllerManager.set_post_composite

function CoreEnvironmentControllerManager:set_post_composite(t, dt)
    old_set_post_composite(self, t, dt)

    local vp = managers.viewport:first_active_viewport()

	if not vp then
		return
    end

    if not alive(self._vp) then
		return
	end

    if easterEgg_active then
        self._vp:vp():set_post_processor_effect("World", Idstring("color_grading_post"), Idstring(easterEgg_effect))
        self._ignore_user_color_grading = true
    else
        local color_grading = self._default_color_grading

        if not self._ignore_user_color_grading then
            color_grading = managers.user:get_setting("video_color_grading") or self._default_color_grading
        end
        self._vp:vp():set_post_processor_effect("World", Idstring("color_grading_post"), Idstring(color_grading))
	end


end


---- 这里控制彩蛋画面 --------













Hooks:PostHook(PlayerManager, "add_grenade_amount", "ghost_haste_throw", function(self, amount)

    if not Utils:IsInHeist() then
        return
    end

    if amount == -1 and managers.player:has_category_upgrade("player","ghost_haste") and is_ghost_haste == -1 then
        --managers.chat:_receive_message(1,'灵魂疾走','开启',Color.red)
        
        --写在这

        if managers.blackmarket:equipped_suit_string() == "ghostly" then
            easterEgg_active = true
            if GhostPerkDeckVinight.settings.gallop_effect == 1 then
                easterEgg_effect = 'color_sin_classic'
            else
                easterEgg_effect = "color_madplanet"
            end
        end





        setplayermaxarmor()
        is_ghost_haste = 0
        function PlayerStandard:_get_max_walk_speed(t, force_run,...)
            return 1100
        end
        
        DelayedCalls:Add( "DelayedCallsHaste", ghost_haste_buffer_CD , function()
            if not Utils:IsInHeist() then
                return
            end
            easterEgg_active = false
            PlayerStandard._get_max_walk_speed = backUpSpeed
        end )


    end

    if amount == 1 and managers.player:has_category_upgrade("player","ghost_save") and is_ghost_save == -1 then
        --managers.chat:_receive_message(1,'灵魂保护','开启',Color.green)

        --写在这
        if managers.blackmarket:equipped_suit_string() == "ghostly" then
            easterEgg_active = true
            easterEgg_effect = "color_bhd"
        end

        
        is_ghost_save = 0
        managers.player:player_unit():character_damage():set_god_mode(true)


        DelayedCalls:Add( "DelayedCallsSave", ghost_save_buffer_CD , function()
            if not Utils:IsInHeist() then
                return
            end
            easterEgg_active = false
            managers.player:player_unit():character_damage():set_god_mode(false)
        end )
    end

end)











---------------hud----------------
local BOX_W = 140 --width of box
local BOX_H = 35 --height of box
local BOX_X = 0 --horizontal position
--log("the X is "..GhostPerkDeckVinight.settings.hud_po_X)
if GhostPerkDeckVinight and GhostPerkDeckVinight.settings.ltyr_hud == 4 or GhostPerkDeckVinight.settings.ltyr_hud == 2 or GhostPerkDeckVinight.settings.ltyr_hud == 1 and WFHud then
    BOX_X = 9999
end
local BOX_Y = 150 --vertical position
local FONT_SIZE = 15 --size of the clock text
local TEXT_COLOR = "ffffff" --hexadecimal representation of the clock text color
local TEXT_H_MARGIN = 8 --horizontal space between the edge of the clock box to the text
local a = 1

if GhostPerkDeckVinight.settings.ltyr_hud == 3 or GhostPerkDeckVinight.settings.ltyr_hud == 1 and not WFHud then
    if GhostPerkDeckVinight then
        BOX_W = GhostPerkDeckVinight.settings.hud_po_W --width of box
        BOX_H = GhostPerkDeckVinight.settings.hud_po_H --height of box
        BOX_X = GhostPerkDeckVinight.settings.hud_po_X --horizontal position
        BOX_Y = GhostPerkDeckVinight.settings.hud_po_Y --vertical position
        FONT_SIZE = GhostPerkDeckVinight.settings.hud_font_size --size of the clock text
        TEXT_H_MARGIN = GhostPerkDeckVinight.settings.hud_text_h --horizontal space between the edge of the clock box to the text
    end
end

-- 以上是指定hud的大小



Hooks:PostHook(HUDManager, "_setup_player_info_hud_pd2","GhostSkillHud", function(self)
    
    if not managers.player:has_category_upgrade("player","ghost_haste") and not managers.player:has_category_upgrade("player","ghost_save") then
        if self._Ghost_spedometer_box then
            self._Ghost_spedometer_box:set_visible(false)
            self._Ghost_spedometer_label:set_visible(false)
        end
        return
    end


    if (GhostPerkDeckVinight.settings.ltyr_hud ~= 3 or GhostPerkDeckVinight.settings.ltyr_hud ~= 1) and WFHud then
        if self._Ghost_spedometer_box then
            --self._Ghost_spedometer_box:set_visible(false)
            --self._Ghost_spedometer_label:set_visible(false)
        end
        --return
    else

        if self._Ghost_spedometer_box then
            self._Ghost_spedometer_box:set_visible(true)
            self._Ghost_spedometer_label:set_visible(true)
        end
    end
	local hm = managers.hud:script(PlayerBase.PLAYER_INFO_HUD_PD2)
	local parent_panel = hm and hm.panel


    function WFHud_adddebuff_DataSave()
        WFHud:add_buff("player", "ghost_haste", DataHaste-WFHud_buff_DataSave)

        if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_haste then
            WFHud.skill_map.player.ghost_haste.is_debuff = false
        end  ------------------------------------------------------------------------------
    end--                                                                                 |
--                                                                                        V

    function set_data()

        local DataSave = ''
        local DataHaste = ''
        local TextData = ''
        local WFHud_buff_DataSave = 3
        local schinese = Idstring("schinese"):key() == SystemInfo:language():key()
        if is_ghost_haste > -1 then

            if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_haste then
                WFHud.skill_map.player.ghost_haste.is_debuff = true
            end

            DataHaste = string.format("%.2f", ghost_haste_CD - is_ghost_haste)
            if GhostPerkDeckVinight.settings.ltyr_hud == 1 then
                if WFHud then
                    WFHud:add_buff("player", "ghost_haste", DataHaste)
                    --WFHud.skill_map.player.ghost_haste.is_debuff = true
                    --DelayedCalls:Add("WFHud_datasave_debuff", WFHud_buff_DataSave, WFHud_adddebuff_DataSave)
                end
            elseif GhostPerkDeckVinight.settings.ltyr_hud == 2 then
                WFHud:add_buff("player", "ghost_haste", DataHaste)
            end
        else
            if GhostPerkDeckVinight.settings.ltyr_hud == 1 and WFHud or GhostPerkDeckVinight.settings.ltyr_hud == 2 then
                WFHud:remove_buff("player", "ghost_haste")
            end
            if schinese then
                DataHaste = '冷却完毕'
            else
                DataHaste = 'OK'
            end
        end

        if is_ghost_save > -1 then
            if WFHud and WFHud.skill_map.player and WFHud.skill_map.player.ghost_save then
                WFHud.skill_map.player.ghost_save.is_debuff = true
            end
            DataSave = string.format("%.2f", ghost_save_CD - is_ghost_save)
            if GhostPerkDeckVinight.settings.ltyr_hud == 1 then
                if WFHud then
                    WFHud:add_buff("player", "ghost_save", DataSave)
                end
            elseif GhostPerkDeckVinight.settings.ltyr_hud == 2 then
                WFHud:add_buff("player", "ghost_save", DataSave)
            end
        else
            if GhostPerkDeckVinight.settings.ltyr_hud == 1 and WFHud or GhostPerkDeckVinight.settings.ltyr_hud == 2 then
                WFHud:remove_buff("player", "ghost_save")
            end

            if schinese then
                DataSave = '冷却完毕'
            else
                DataSave = 'OK'
            end

        end

        if schinese then
            TextData = '灵魂疾走冷却:' .. DataHaste ..'\n灵魂保护冷却:' .. DataSave
        else
            TextData = 'Gallop:' .. DataHaste ..'\nSpirit Shield:' .. DataSave
        end

        self._Ghost_spedometer_label:set_text(TextData)
    end
    

	if alive(parent_panel) then
        self._Ghost_spedometer_box = HUDBGBox_create(parent_panel, {
            name = "Ghostspedometer_box",
            w = BOX_W,
            h = BOX_H,
            x = BOX_X,
            y = BOX_Y
        }, {
            blend_mode = "add"
        })
    end
    self._Ghost_spedometer_label = self._Ghost_spedometer_box:text({
        name = "Ghostspedometer_label",
        color = Color(TEXT_COLOR),
        font = tweak_data.hud_players.ammo_font,
        text = "",
        vertical = "center",
        align = "left",
        x = TEXT_H_MARGIN,
        layer = 1,
        font_size = FONT_SIZE
    })
	self:add_updator("Ghostspedometer_update", set_data)

end)