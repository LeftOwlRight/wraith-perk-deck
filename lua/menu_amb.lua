
dofile(ModPath .. "automenubuilder.lua")

GhostPerkDeckVinight = GhostPerkDeckVinight or {
  
  settings = {
    ltyr_hud = 1,
    gallop_effect = 1,
    hud_po_X = 0,
    hud_po_Y = 150,
    hud_po_H = 35,
    hud_po_W = 140,
    hud_font_size = 15,
    hud_text_h = 8,
    },

  values = {
    hud_po_X = {-500, 2000, 0.1},
    hud_po_Y = {-2000, 2000, 0.1},
    hud_po_H = {-50, 2000, 0.1},
    hud_po_W = {-50, 2000, 0.1},
    hud_font_size = {0, 50, 1},
    hud_text_h = {-20, 200, 0.1},
    ltyr_hud = { "GPDV_auto_detect", "GPDV_wfhud", "GPDV_vanilla", "GPDV_none" },
    gallop_effect = { "color_sin_classic", "color_madplanet" }
    },

  order = {
    gallop_effect = 100,
    ltyr_hud = 99,
    hud_po_X = 98,
    hud_po_Y = 97,
    hud_po_H = 96,
    hud_po_W = 95,
    hud_font_size = 93,
    hud_text_h = 94
    }
}


AutoMenuBuilder:load_settings(GhostPerkDeckVinight.settings, "ghost_perk_deck_vinight")
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusGhostPerkDeckVinight", function(menu_manager, nodes)

  -- loads previously saved settings for that mod into the given table
  --AutoMenuBuilder:load_settings(GhostPerkDeckVinight.settings, "ghost_perk_deck_vinight")

  AutoMenuBuilder:create_menu_from_table(nodes, GhostPerkDeckVinight.settings, "ghost_perk_deck_vinight", "blt_options", GhostPerkDeckVinight.values, GhostPerkDeckVinight.order )

end)




