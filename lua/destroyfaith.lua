

Hooks:PreHook(PlayerDamage, "damage_bullet", "destroyfaithprehook", function(self,attack_data)
    local randomDamage = math.random()
    local randomDamage_yu = randomDamage % 0.01
    local randomDamage_t = randomDamage - randomDamage_yu
    
    if managers.player:has_category_upgrade("player","destroy_faith") and randomDamage < 0.75 then
        attack_data.damage = attack_data.damage * randomDamage
        if GhostPerkDeckVinight.settings.ltyr_hud == 1 then
            if WFHud then
                WFHud:add_buff("player", "destroy_faith", randomDamage_t)
            end
        elseif GhostPerkDeckVinight.settings.ltyr_hud == 2 then
            WFHud:add_buff("player", "destroy_faith", randomDamage_t)
        end
    elseif managers.player:has_category_upgrade("player","destroy_faith") then
        if GhostPerkDeckVinight.settings.ltyr_hud == 1 or GhostPerkDeckVinight.settings.ltyr_hud == 2 then
            if WFHud then
                WFHud:remove_buff("player", "destroy_faith")
            end
        end
    end

    if managers.player:has_category_upgrade("player","destroy_faith") and attack_data.attacker_unit:base()._tweak_table == "sniper" then
        attack_data.damage = managers.player:player_unit():character_damage():get_real_armor()/2
    end
    
end )


