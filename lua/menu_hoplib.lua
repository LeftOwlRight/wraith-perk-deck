
GhostPerkDeckVinight = {}

GhostPerkDeckVinight.settings = {
    gallop_effect = 1,
    ltyr_hud = 1,
    hud_po_X = 0,
    hud_po_Y = 150,
    hud_po_H = 35,
    hud_po_W = 140,
    hud_font_size = 15,
    hud_text_h = 8
    }

GhostPerkDeckVinight.values = {
    ltyr_hud = {
      items = {"GPDV_auto_detect", "GPDV_wfhud", "GPDV_vanilla", "GPDV_none" },
      priority = 99
    },
    gallop_effect = {
      items = { "color_sin_classic", "color_madplanet" },
      priority = 100
    },
    hud_po_X = {
      min = -500,
      max = 2000,
      step = 0.1,
      priority = 98
    },
    hud_po_Y = {
      min = -2000,
      max = 2000,
      step = 0.1,
      priority = 97
    },
    hud_po_H = {
      min = -50,
      max = 2000,
      step = 0.1,
      priority = 96
    },
    hud_po_W = {
      min = -50,
      max = 2000,
      step = 0.1,
      priority = 95
    },
    hud_font_size = {
      min = 0,
      max = 50,
      step = 1,
      priority = 93
    },
    hud_text_h = {
      min = -20,
      max = 200,
      step = 0.1,
      priority = 94
    }
  }



local builder = MenuBuilder:new("ghost_perk_deck_vinight", GhostPerkDeckVinight.settings, GhostPerkDeckVinight.values)
Hooks:Add("MenuManagerBuildCustomMenus", "MenuManagerBuildCustomMenusGhostPerkDeckVinight", function(menu_manager, nodes)
  builder:create_menu(nodes)
end)




