Hooks:PostHook(UpgradesTweakData,"_player_definitions","ghostskill_player_definitions",function(self)


	self.values.player.ghost_antiexp = {
		MadeBy = 'vinight'
	}
	self.values.player.ghost_haste = {
	
	}
	self.values.player.destroy_faith = {
		
	}
	self.values.player.ghost_save = {
	
	}
	self.values.player.ghost_galloped = {
	
	}
	
	self.definitions.ghost_skill_1_1 = {
		name_id = "menu_deck_ghost_vinight_1_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ghost_antiexp",
			category = "player"
		}
	}
	self.definitions.ghost_skill_2 = {
		name_id = "menu_deck_ghost_vinight_3_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ghost_haste",
			category = "player"
		}
	}
	self.definitions.ghost_skill_3 = {
		name_id = "menu_deck_ghost_vinight_5_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "destroy_faith",
			category = "player"
		}
	}
	self.definitions.ghost_skill_4 = {
		name_id = "menu_deck_ghost_vinight_7_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ghost_save",
			category = "player"
		}
	}
	self.definitions.ghost_skill_5 = {
		name_id = "menu_deck_ghost_vinight_9_title",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ghost_galloped",
			category = "player"
		}
	}








end)


local old_mrwi_deck9_options = UpgradesTweakData.mrwi_deck9_options
function UpgradesTweakData.mrwi_deck9_options()
	local deck9_options = old_mrwi_deck9_options()	
	table.insert(deck9_options, {
		desc_id = "menu_deck_ghost_vinight_7_desc",
		short_id = "menu_deck_ghost_vinight_7_title",
		tier = 1,
		tree = 22,
		upgrades = {
			"ghost_skill_4",
			"player_perk_armor_regen_timer_multiplier_4"
		}
	})

	table.insert(deck9_options, {
		desc_id = "menu_deck_ghost_vinight_3_desc",
		short_id = "menu_deck_ghost_vinight_3_title",
		tier = 1,
		tree = 22,
		upgrades = {
			"ghost_skill_2"
		}
	})
	return deck9_options
end