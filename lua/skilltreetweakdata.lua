
local paid = false
--if you want this perk deck to be instantly unlocked, change the above "true" to "false" (no quotation marks)



Hooks:PostHook(SkillTreeTweakData,"init","ghostSkill",function(self,data)
	local costs = {
		[1] = 200,
		[2] = 300,
		[3] = 400,
		[4] = 600,
		[5] = 1000,
		[6] = 1600,
		[7] = 2400,
		[8] = 3200,
		[9] = 4000
	}
	local function cost(n)
		return paid and costs[n] or 0
	end

	local deck2 = {
		cost = cost(2),
		name_id = "menu_deckall_2",
		desc_id = "menu_deckall_fixed_2_desc",
		upgrades = {
			"weapon_passive_headshot_damage_multiplier"
		},
		icon_xy = {
			1,
			0
		}
	}
	local deck4 = {
		cost = cost(4),
		name_id = "menu_deckall_4",
		desc_id = "menu_deckall_fixed_4_desc",
		upgrades = {
			"passive_player_xp_multiplier",
			"player_passive_suspicion_bonus",
			"player_passive_armor_movement_penalty_multiplier"
		},
		icon_xy = {
			3,
			0
		}
	}
	local deck6 = {
		cost = cost(6),
		name_id = "menu_deckall_6",
		desc_id = "menu_deckall_fixed_6_desc",
		upgrades = {
			"armor_kit",
			"player_pick_up_ammo_multiplier"
		},
		icon_xy = {
			5,
			0
		}
	}
	local deck8 = {
		cost = cost(8),
		name_id = "menu_deckall_8",
		desc_id = "menu_deckall_fixed_8_desc",
		upgrades = {
			"weapon_passive_damage_multiplier",
			"passive_doctor_bag_interaction_speed_multiplier"
		},
		icon_xy = {
			7,
			0
		}
	}



	-- 自定义部分 13579
	table.insert(self.specializations,
		{
			name_id = "menu_deck_ghost_vinight_title",
			desc_id = "menu_deck_ghost_vinight_desc",

			{
				upgrades = {
					"ghost_skill_1",
					"ghost_skill_1_1",
					"player_passive_dodge_chance_2",
					"player_camouflage_multiplier"
				},
				cost = cost(1),
				icon_xy = {3, 0}, --
				name_id = "menu_deck_ghost_vinight_1_title",
				desc_id = "menu_deck_ghost_vinight_1_desc"
			},
			
			deck2,
			{
				upgrades = {
					"ghost_skill_2"
				},
				cost = cost(3),
				icon_xy = {1, 2}, --
				name_id = "menu_deck_ghost_vinight_3_title",
				desc_id = "menu_deck_ghost_vinight_3_desc"
			},
			deck4,
			{
				upgrades = {
					"ghost_skill_3",
					"player_killshot_close_panic_chance"
				},
				cost = cost(5),
				icon_xy = {3, 1}, --
				name_id = "menu_deck_ghost_vinight_5_title",
				desc_id = "menu_deck_ghost_vinight_5_desc"
			},
			deck6,
			{
				upgrades = {
					"ghost_skill_4",
					"player_perk_armor_regen_timer_multiplier_4"
				},
				cost = cost(7),
				icon_xy = {4, 3},--
				name_id = "menu_deck_ghost_vinight_7_title",
				desc_id = "menu_deck_ghost_vinight_7_desc"
			},
			deck8,
			{
				upgrades = {
					"ghost_skill_5"
				},
				cost = cost(9),
				icon_xy = {4, 4},
				name_id = "menu_deck_ghost_vinight_9_title",
				desc_id = "menu_deck_ghost_vinight_9_desc"
			}
		}
	)
end)